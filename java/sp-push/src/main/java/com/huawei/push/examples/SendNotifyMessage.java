/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.huawei.push.examples;

import com.huawei.push.android.AndroidNotification;
import com.huawei.push.android.ClickAction;
import com.huawei.push.exception.HuaweiMesssagingException;
import com.huawei.push.message.AndroidConfig;
import com.huawei.push.message.Message;
import com.huawei.push.message.Notification;
import com.huawei.push.messaging.HuaweiApp;
import com.huawei.push.messaging.HuaweiMessaging;
import com.huawei.push.model.Importance;
import com.huawei.push.model.Urgency;
import com.huawei.push.model.Visibility;
import com.huawei.push.reponse.SendResponse;
import com.huawei.push.util.InitAppUtils;

import java.util.HashMap;
import java.util.List;

public class SendNotifyMessage {
    /**
     * send notification message
     * 通知消息
     *
     * @throws HuaweiMesssagingException
     */
    public static String appId;
    public static String appSecret;

    public void sendNotification(List<String> tokens, String title, String content, HashMap<String, String> maps, String appId, String appSecret) throws HuaweiMesssagingException {
        SendNotifyMessage.appId = appId;
        SendNotifyMessage.appSecret = appSecret;
        HuaweiApp app = InitAppUtils.initializeApp();
        HuaweiMessaging huaweiMessaging = HuaweiMessaging.getInstance(app);
        Notification notification = Notification.builder().setTitle(title)
                .setBody(content)
                .build();

        AndroidNotification androidNotification = AndroidNotification.builder().setIcon("/raw/ic_launcher2")
                .setColor("#AACCDD")
                .setDefaultSound(true)
                .setClickAction(ClickAction.builder().setType(1).setAction("com.push.demo.internal").build())
                .setStyle(1)
                .setBigTitle(title)
                .setBigBody(content)
                .setNotifyId(486)
                .setImportance(Importance.LOW.getValue())
                .setVisibility(Visibility.PUBLIC.getValue())
                .setForegroundShow(true)
                .build();
        AndroidConfig androidConfig;
        if (maps != null) {
            androidConfig = AndroidConfig.builder().setCollapseKey(-1)
                    .setUrgency(Urgency.HIGH.getValue())
                    .setTtl("10000s")
                    .setData(maps.toString())
                    .setBiTag("the_sample_bi_tag_for_receipt_service")
                    .setNotification(androidNotification)
                    .build();
        } else {
            androidConfig = AndroidConfig.builder().setCollapseKey(-1)
                    .setUrgency(Urgency.HIGH.getValue())
                    .setTtl("10000s")
                    .setBiTag("the_sample_bi_tag_for_receipt_service")
                    .setNotification(androidNotification)
                    .build();
        }


        Message message = Message.builder().setNotification(notification)
                .setAndroidConfig(androidConfig)
                .addAllToken(tokens)
                .build();
        SendResponse response = huaweiMessaging.sendMessage(message);
        if (response.getCode().equals("80000000")) {//成功

        } else {//失败

        }
    }
}
