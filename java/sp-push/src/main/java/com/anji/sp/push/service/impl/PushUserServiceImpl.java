package com.anji.sp.push.service.impl;

import com.anji.sp.enums.IsDeleteEnum;
import com.anji.sp.enums.RepCodeEnum;
import com.anji.sp.enums.UserStatus;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.common.DeviceType;
import com.anji.sp.push.mapper.PushUserMapper;
import com.anji.sp.push.model.po.PushUserPO;
import com.anji.sp.push.model.vo.PushUserVO;
import com.anji.sp.push.service.PushUserService;
import com.anji.sp.util.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Service
public class PushUserServiceImpl extends ServiceImpl<PushUserMapper, PushUserPO> implements PushUserService {

    @Autowired
    private PushUserMapper pushUserMapper;

    /**
     * 根据数据库必填项，校验是否为空，不校验主键
     *
     * @param pushUserVO
     * @return
     */
    private ResponseModel validateCreateFieldNotNull(PushUserVO pushUserVO) {
        if (pushUserVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        if (pushUserVO.getDeviceId() == null) {
            return RepCodeEnum.NULL_ERROR.parseError("deviceId");
        }
        if (pushUserVO.getDeviceType() == null) {
            return RepCodeEnum.NULL_ERROR.parseError("deviceType");
        }
        if (pushUserVO.getManuToken() == null && pushUserVO.getRegistrationId() == null) {
            return RepCodeEnum.NULL_ERROR.parseError("请输入token或registrationId");
        }
        /* 该片段由生成器产生，请根据实际情况修改
        if(pushUserVO.getId() == null){
            return RepCodeEnum.NULL_ERROR.parseError("id");
        }
        if(StringUtils.isBlank(pushUserVO.getAppKey())){
            return RepCodeEnum.NULL_ERROR.parseError("appKey");
        }
        if(StringUtils.isBlank(pushUserVO.getDeviceId())){
            return RepCodeEnum.NULL_ERROR.parseError("deviceId");
        }
        if(StringUtils.isBlank(pushUserVO.getManuToken())){
            return RepCodeEnum.NULL_ERROR.parseError("manuToken");
        }
        if(StringUtils.isBlank(pushUserVO.getDeviceType())){
            return RepCodeEnum.NULL_ERROR.parseError("deviceType");
        }
        if(StringUtils.isBlank(pushUserVO.getAlias())){
            return RepCodeEnum.NULL_ERROR.parseError("alias");
        }
        if(StringUtils.isBlank(pushUserVO.getRegistrationId())){
            return RepCodeEnum.NULL_ERROR.parseError("registrationId");
        }
        if(StringUtils.isBlank(pushUserVO.getBrand())){
            return RepCodeEnum.NULL_ERROR.parseError("brand");
        }
        if(StringUtils.isBlank(pushUserVO.getOsVersion())){
            return RepCodeEnum.NULL_ERROR.parseError("osVersion");
        }
        if(pushUserVO.getEnableFlag() == null){
            return RepCodeEnum.NULL_ERROR.parseError("enableFlag");
        }
        if(pushUserVO.getDeleteFlag() == null){
            return RepCodeEnum.NULL_ERROR.parseError("deleteFlag");
        }
        if(pushUserVO.getCreateBy() == null){
            return RepCodeEnum.NULL_ERROR.parseError("createBy");
        }
        if(pushUserVO.getCreateDate() == null){
            return RepCodeEnum.NULL_ERROR.parseError("createDate");
        }
        if(pushUserVO.getUpdateBy() == null){
            return RepCodeEnum.NULL_ERROR.parseError("updateBy");
        }
        if(pushUserVO.getUpdateDate() == null){
            return RepCodeEnum.NULL_ERROR.parseError("updateDate");
        }
        if(StringUtils.isBlank(pushUserVO.getRemarks())){
            return RepCodeEnum.NULL_ERROR.parseError("remarks");
        }
        */
        return ResponseModel.success();
    }

    @Override
    public ResponseModel initUserInfo(PushUserVO pushUserVO) {
        //根据deviceId查询是否已经添加数据
        ResponseModel responseModel = queryByDeviceId(pushUserVO);
        //查询到 更改数据
        if (responseModel.isSuccess()) {
            return updateByDeviceId(pushUserVO);
        } else {
            //查不到添加数据
            return create(pushUserVO);
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseModel create(PushUserVO pushUserVO) {
        //参数校验
        ResponseModel valid = validateCreateFieldNotNull(pushUserVO);
        if (valid.isError()) {
            return valid;
        }
        //业务校验
        //...todo

        //业务处理
        //业务处理
        if (pushUserVO.getEnableFlag() == null) {
            pushUserVO.setEnableFlag(UserStatus.OK.getIntegerCode());
        }
        pushUserVO.setDeleteFlag(IsDeleteEnum.NOT_DELETE.getIntegerCode());
        pushUserVO.setUpdateBy(1L);
        pushUserVO.setCreateDate(LocalDateTime.now());
        pushUserVO.setUpdateBy(1L);
        pushUserVO.setUpdateDate(LocalDateTime.now());

        PushUserPO pushUserPO = new PushUserPO();
        BeanUtils.copyProperties(pushUserVO, pushUserPO);
        boolean flag = save(pushUserPO);
        //返回结果
        if (flag) {
            return ResponseModel.successData(pushUserPO);
        } else {
            return ResponseModel.errorMsg(RepCodeEnum.ERROR);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseModel updateByDeviceId(PushUserVO pushUserVO) {
        //参数校验
        if (pushUserVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        String deviceId = pushUserVO.getDeviceId();
        String appKey=pushUserVO.getAppKey();
        if (deviceId == null) {
            return RepCodeEnum.NULL_ERROR.parseError("deviceId");
        }
        if (appKey == null) {
            return RepCodeEnum.NULL_ERROR.parseError("appKey");
        }
        //业务校验
        //...todo

        //业务处理
        QueryWrapper<PushUserPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("device_id", deviceId);
        queryWrapper.eq("app_key", appKey);
        PushUserPO pushUserPO = getOne(queryWrapper);
        if (pushUserPO == null) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("数据");
        }
        pushUserVO.setUpdateBy(1L);
        pushUserVO.setUpdateDate(LocalDateTime.now());
        BeanUtils.copyProperties(pushUserVO, pushUserPO, true);
        boolean flag = updateById(pushUserPO);
        //返回结果
        if (flag) {
            return ResponseModel.successData(pushUserPO);
        } else {
            return ResponseModel.errorMsg("修改失败");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseModel deleteByDeviceId(PushUserVO pushUserVO) {
        //参数校验
        if (pushUserVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        String deviceId = pushUserVO.getDeviceId();
        String appKey=pushUserVO.getAppKey();
        if (deviceId == null) {
            return RepCodeEnum.NULL_ERROR.parseError("deviceId");
        }
        if (appKey == null) {
            return RepCodeEnum.NULL_ERROR.parseError("appKey");
        }

        //业务处理
        QueryWrapper<PushUserPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("device_id", deviceId);
        queryWrapper.eq("app_key", appKey);
        PushUserPO pushUserPO = getOne(queryWrapper);
        if (pushUserPO == null) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("数据");
        }
        boolean flag = removeById(pushUserPO);

        //返回结果
        if (flag) {
            return ResponseModel.successData("删除成功");
        } else {
            return ResponseModel.errorMsg("删除失败");
        }
    }

    @Override
    public ResponseModel queryByDeviceId(PushUserVO pushUserVO) {
        //参数校验
        if (pushUserVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        String deviceId = pushUserVO.getDeviceId();
        String appKey=pushUserVO.getAppKey();
        if (deviceId == null) {
            return RepCodeEnum.NULL_ERROR.parseError("deviceId");
        }
        if (appKey == null) {
            return RepCodeEnum.NULL_ERROR.parseError("appKey");
        }
        //业务处理
        QueryWrapper<PushUserPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("device_id", deviceId);
        queryWrapper.eq("app_key",appKey);
        PushUserPO pushUserPO = getOne(queryWrapper);
        if (pushUserPO == null) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("数据");
        }
        return ResponseModel.successData(pushUserPO);
    }

    @Override
    public ResponseModel queryByMaunToken(PushUserVO pushUserVO) {
        //参数校验
        if (pushUserVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        String manuToken = pushUserVO.getManuToken();
        String appKey=pushUserVO.getAppKey();
        if (manuToken == null) {
            return RepCodeEnum.NULL_ERROR.parseError("manuToken");
        }
        if (appKey == null) {
            return RepCodeEnum.NULL_ERROR.parseError("appKey");
        }
        //业务处理
        QueryWrapper<PushUserPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("manu_token", manuToken);
        queryWrapper.eq("app_key",appKey);
        PushUserPO pushUserPO = getOne(queryWrapper);
        if (pushUserPO == null) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("数据");
        }
        return ResponseModel.successData(pushUserPO);
    }

    @Override
    public ResponseModel queryByDeviceIds(ArrayList<String> deviceIds,String appKey) {
        if (deviceIds == null || deviceIds.size() == 0) {
            return RepCodeEnum.NULL_ERROR.parseError("deviceIds");
        }
        //业务处理
        QueryWrapper<PushUserPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("device_id", deviceIds);
        queryWrapper.eq("app_key",appKey);
        List<PushUserPO> pushUserPOS = list(queryWrapper);
        if (pushUserPOS == null) {
            return ResponseModel.successData(new ArrayList<PushUserPO>());
        }
        return ResponseModel.successData(pushUserPOS);
    }

    //返回结果

    @Override
    public ResponseModel queryByPage(PushUserVO pushUserVO) {
        //参数校验
        if (pushUserVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        //...todo

        //分页参数
        Page<PushUserVO> page = new Page<PushUserVO>(pushUserVO.getPageNo(), pushUserVO.getPageSize());
        pushUserVO.setDeleteFlag(IsDeleteEnum.NOT_DELETE.getIntegerCode());

        //业务处理
        IPage<PushUserVO> pageList = pushUserMapper.queryByPage(page, pushUserVO);

        //返回结果
        return ResponseModel.successData(pageList);
    }

    //根据deviceTyep 查询华为 小米 oppo vivo ios 用户
    @Override
    public ResponseModel queryByDeviceType(String deviceType,String appKey) {
        //业务处理
        QueryWrapper<PushUserPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("device_type", deviceType);
        queryWrapper.eq("app_key", appKey);
        if (!deviceType.equals(DeviceType.IOS)) {
            queryWrapper.isNotNull("manu_token");
        }
        List<PushUserPO> pushUserPOS = list(queryWrapper);
        if (pushUserPOS == null) {
            return ResponseModel.successData(new ArrayList<PushUserPO>());
        }
        return ResponseModel.successData(pushUserPOS);
    }

    //查询全部 用户
    @Override
    public ResponseModel queryAll(String appKey) {
        //业务处理
        QueryWrapper<PushUserPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("app_key",appKey);
        List<PushUserPO> pushUserPOS = list(queryWrapper);
        if (pushUserPOS == null || pushUserPOS.size() == 0) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("数据");
        }
        return ResponseModel.successData(pushUserPOS);

    }


    //查询其他手机用户以及除了ios manu_token为空的用户
    @Override
    public ResponseModel queryOther(String appKey) {
        //业务处理
        QueryWrapper<PushUserPO> queryWrapper = new QueryWrapper<>();
        ArrayList<String> deviceTypes = new ArrayList<String>();
        deviceTypes.add("5");
        queryWrapper.notIn("device_type", deviceTypes);
        queryWrapper.eq("manu_token","");
        queryWrapper.eq("app_key",appKey);
        List<PushUserPO> pushUserPOS = list(queryWrapper);
        if (pushUserPOS == null) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("数据");
        }
        return ResponseModel.successData(pushUserPOS);
    }
}
