package com.anji.sp.push.model.vo;

import java.io.Serializable;

/**
 * Vivo推送回调Bean
 */
public class VivoCallBackBean implements Serializable {
    private static final long serialVersionUID = 1L;

    private String param;
    private String targets;//"regId1,regId2,regId3"

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getTargets() {
        return targets;
    }

    public void setTargets(String targets) {
        this.targets = targets;
    }

/**
 * {
 *
 *       "taskId1":{
 *
 *               "param":"param1",
 *
 *               "targets":"alias1,alias2,alias3"
 *
 *     },
 *
 *       "taskId2":{
 *
 *               "param":"param2",
 *
 *               "targets":"regId1,regId2,regId3"
 *
 *     }
 *
 * }
 */
}
