package com.anji.plus.ajpushlibrary.service;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 *
 * </p>
 */
public interface IAppSpService {
    void putPushInfo();
}
